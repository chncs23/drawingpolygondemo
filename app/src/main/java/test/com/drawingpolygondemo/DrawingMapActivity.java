package test.com.drawingpolygondemo;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import java.util.Objects;

import test.com.drawingpolygondemo.map.fragment.MapDrawingFragment;

public class DrawingMapActivity extends FragmentActivity {

    private TestDrawingPolygonFragment drawingPolygonFragment; // Might be null if Google Play services APK is not available.
    FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawing_map);
        mFragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            drawingPolygonFragment = new TestDrawingPolygonFragment();
            mFragmentManager.beginTransaction()
                    .add(R.id.map_container, drawingPolygonFragment, MapDrawingFragment.FRAGMENT_TAG)
                    .disallowAddToBackStack()
                    .commit();
        }
    }

    public static class TestDrawingPolygonFragment extends MapDrawingFragment<Objects>{

        @Override
        public boolean isGeoValueValid() {
            return false;
        }

        @Override
        public Objects getData() {
            return null;
        }

        @Override
        public boolean onNavigationItemSelected(int itemPosition, long itemId) {
            return false;
        }
    }


}
