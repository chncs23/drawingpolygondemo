package test.com.drawingpolygondemo.map.fragment;


import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.maps.android.PolyUtil;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import test.com.drawingpolygondemo.R;
import test.com.drawingpolygondemo.map.utils.MapUtils;
import test.com.drawingpolygondemo.map.utils.RaiToSqMeterConvertor;

/**
 * @param <T> Class to get As data of Fragment
 */

public abstract class MapDrawingFragment<T> extends Fragment implements ActionBar.OnNavigationListener, OnMapReadyCallback, View.OnClickListener {

    public static final int POLYGON_MODE = 1;

    public static final String FRAGMENT_TAG = "drawing_map";

    FragmentManager mFragmentManager;
    protected DrawingPolygonFragment polygonFragment;

    protected TextView currentLocationTxtview, areaTxtview;

    protected ToggleButton drawingModeToggle;

    protected Button clearPolygon;
    protected ImageButton drawOnLocation, backToMainArea;

    DecimalFormat df = new DecimalFormat("#.##");

    protected JsonArray mRawBackGroundPolygon, mRawDrawingPolygon;
    protected LatLng mPoint, mLastPointAdded;
    protected CameraPosition mCameraPosition;

    protected int drawingMode;
    protected double mArea;
    protected boolean isMapLocked = false;
    protected boolean isNewActivityCreated = false;

    protected List<LatLng> mDrawingPolygonPoints = new ArrayList<>();
    protected List<List<LatLng>> mDrawingPolygonHoles = new ArrayList<>();
    private android.widget.FrameLayout maparea;

    public abstract boolean isGeoValueValid();

    public abstract T getData();

    boolean skip = false;

    public void onSkip() {
        skip = true;
    }

    public void onNext() {
        skip = false;
    }

    public boolean isSkip() {
        return skip;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawing_geo_area, null);
        currentLocationTxtview = (TextView) view.findViewById(R.id.current_location_txtview);
        areaTxtview = (TextView) view.findViewById(R.id.area_txtview);
        drawOnLocation = (ImageButton) view.findViewById(R.id.draw_on_location);
        backToMainArea = (ImageButton) view.findViewById(R.id.back_to_main_area);
        drawingModeToggle = (ToggleButton) view.findViewById(R.id.drawing_mode_toggle);
        clearPolygon = (Button) view.findViewById(R.id.clear_polygon);

        drawOnLocation.setOnClickListener(this);
        backToMainArea.setOnClickListener(this);
        clearPolygon.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFragmentManager = getChildFragmentManager();
        isNewActivityCreated = savedInstanceState == null;

        usingDrawingPolygonMode();
    }


    public void usingDrawingPolygonMode() {
        drawingMode = POLYGON_MODE;

        FragmentTransaction ft = mFragmentManager.beginTransaction();

        polygonFragment = (DrawingPolygonFragment) mFragmentManager.findFragmentByTag(DrawingPolygonFragment.FRAGMENT_TAG);

        if (mDrawingPolygonPoints == null || mDrawingPolygonPoints.isEmpty()) {
            mDrawingPolygonPoints = MapUtils.getMainPolygon(mRawDrawingPolygon);
            mDrawingPolygonHoles = MapUtils.getPolygonHoles(mRawDrawingPolygon);
        }

        //Log.d("polygon point",mDrawingPolygonPoints.size() + "");

        if (polygonFragment == null) {
            Log.d("polygon frag is null", "m");
            polygonFragment = DrawingPolygonFragment.newInstance(
                    mRawBackGroundPolygon, mDrawingPolygonPoints, mDrawingPolygonHoles,
                    mCameraPosition);
        }

        setAreaText(mArea);

        polygonFragment.setOnPositionChangeListener(new MainSupportMapFragment.OnPositionChangeListener() {
            @Override
            public void onPositionChange(LatLng position) {
                currentLocationTxtview.setText(position.latitude + "," + position.longitude);
            }
        });

        polygonFragment.setOnCameraChangeListener(new MainSupportMapFragment.OnCurrentCameraChangeListener() {
            @Override
            public void setOnCameraChangeListener(CameraPosition position) {
                mCameraPosition = position;
            }
        });

        polygonFragment.setOnPolygonChangeListener(new DrawingPolygonFragment.OnPolygonChangeListener() {
            @Override
            public void onPolygonChange(List<LatLng> polygonArray, List<List<LatLng>> polygonHolesArray, LatLng centroid, double area) {
                mDrawingPolygonPoints = polygonArray;
                mDrawingPolygonHoles = polygonHolesArray;
                mArea = area;
                mPoint = centroid;
                setAreaText(mArea);
            }
        });

        polygonFragment.setOnPositionAddedListener(new DrawingPolygonFragment.OnPositionAddedListener() {
            @Override
            public void onPositionAdded(LatLng lastPoint) {
                mLastPointAdded = lastPoint;
                getActivity().invalidateOptionsMenu();
            }
        });

        polygonFragment.getMapAsync(this);

        ft.replace(R.id.map_area, polygonFragment, DrawingPolygonFragment.FRAGMENT_TAG);
        ft.commit();
    }

    public void setAreaText(double area) {
        if (area > 0) {
            areaTxtview.setText(RaiToSqMeterConvertor.parseArea((int) area) + " (" + df.format(area) + " ตร.ม.)");
        } else {
            areaTxtview.setText("-");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.draw_on_location:
                Location location;
                LatLng currentPoint;
                location = polygonFragment.getCurrentLocation();
                currentPoint = getCurrentPosition(location);
                if (currentPoint == null) {
                    return;
                } else {
                    polygonFragment.drawEachPointOfPolygon(currentPoint);
                }
                break;

            case R.id.back_to_main_area:
                polygonFragment.zoomOnPolygon(null, mRawBackGroundPolygon);
                break;

            case R.id.clear_polygon:

                polygonFragment.clearDrawingMap();
                break;
        }
    }


    public LatLng getCurrentPosition(Location location) {
        LatLng currentPoint;
        if (location == null) {
            Toast.makeText(getActivity(), getResources().getString(R.string.location_notfound), Toast.LENGTH_SHORT).show();
            return null;
        } else {
            currentPoint = new LatLng(location.getLatitude(), location.getLongitude());
            return currentPoint;
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        drawingModeToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isMapLocked = isChecked;
                setMarkerMapLocked(googleMap, isChecked);
            }
        });
        drawingModeToggle.setChecked(isMapLocked);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        boolean available = activeNetworkInfo != null
                && activeNetworkInfo.isConnected();

        return available;
    }

    public void setPolygonMapLocked(GoogleMap googleMap, boolean isChecked) {
        if (isChecked) {
            googleMap.getUiSettings().setScrollGesturesEnabled(false);
        } else {
            googleMap.getUiSettings().setScrollGesturesEnabled(true);

        }
    }

    public void setMarkerMapLocked(GoogleMap googleMap, boolean isChecked) {
        if (isChecked) {
            googleMap.getUiSettings().setScrollGesturesEnabled(false);
        } else {
            googleMap.getUiSettings().setScrollGesturesEnabled(true);

        }
    }

    protected boolean validatePolygon() {
        if (mDrawingPolygonPoints != null && mDrawingPolygonPoints.size() > 2) {
            return true;
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.please_draw_more_than_3_point), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    protected boolean validatePosition() {
        if (mPoint == null) {
            Toast.makeText(getActivity(), getResources().getString(R.string.please_define_position), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    protected boolean validateInsideBoundary() {
        if (mRawBackGroundPolygon == null)
            return true;

        List<LatLng> backgroundPolygon = MapUtils.getMainPolygon(mRawBackGroundPolygon);
        return validateDrawingPolygonInsideStaticPolygon(mDrawingPolygonPoints, backgroundPolygon);
    }

    public boolean validatePointInsideStaticPolygon(LatLng point, List<LatLng> backgroundPolygon) {
        if (notContainLocation(point, backgroundPolygon)) {
            Toast.makeText(getActivity(), "พิกัดที่กำหนดอยู่นอกขอบเขต", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public boolean validateDrawingPolygonInsideStaticPolygon(List<LatLng> drawingPolygon, List<LatLng> backgroundPolygon) {
        for (LatLng eachPoint : drawingPolygon) {
            if (notContainLocation(eachPoint, backgroundPolygon)) {
                Toast.makeText(getActivity(), "พิกัดบางจุดที่กำหนดอยู่นอกขอบเขต", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    protected boolean notContainLocation(LatLng point, List<LatLng> backgroundPolygon) {
        return !PolyUtil.containsLocation(point, backgroundPolygon, false) &&
                !PolyUtil.isLocationOnEdge(point, backgroundPolygon, false, 0.5);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(DrawingPolygonFragment.DRAWING_POLYGON, (ArrayList<? extends Parcelable>) mDrawingPolygonPoints);
        outState.putSerializable(DrawingPolygonFragment.DRAWING_POLYGON_HOLES, (Serializable) mDrawingPolygonHoles);
        outState.putParcelable("marking_point", mPoint);
        outState.putParcelable("camera_positon", mCameraPosition);
        outState.putBoolean("locked_toggle", drawingModeToggle.isChecked());
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mDrawingPolygonHoles = (List<List<LatLng>>) savedInstanceState.getSerializable(DrawingPolygonFragment.DRAWING_POLYGON_HOLES);
            mPoint = savedInstanceState.getParcelable("marking_point");
            mCameraPosition = savedInstanceState.getParcelable("camera_positon");
            isMapLocked = savedInstanceState.getBoolean("locked_toggle");
        }
    }
}
