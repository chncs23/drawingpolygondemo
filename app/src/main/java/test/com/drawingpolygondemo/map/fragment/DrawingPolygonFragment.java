/*  TAMIS Android Project 
 *
 *  ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 * Copyright (C) 2557-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package test.com.drawingpolygondemo.map.fragment;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.gson.JsonArray;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.ui.IconGenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import test.com.drawingpolygondemo.map.utils.MapUtils;

/**
 * add description here!
 *
 * @author N. Choatravee
 * @version 1.0
 */
public class DrawingPolygonFragment extends MainSupportMapFragment {

    public static final String CAMERA_POSITION = "cam_pos";
    public static final String DRAWING_POLYGON = "drawing_polygon";
    public static final String DRAWING_POLYGON_HOLES = "drawing_polygon_holes";

    public static final String FRAGMENT_TAG = "drawing_polygon";

    GoogleMap mMap;
    OnPolygonChangeListener mAreaChangedListener;
    OnPositionAddedListener mPointListener;

    private boolean editMode;

    protected CameraPosition mCameraPosition;

    JsonArray rawMainPolygon;


    private List<LatLng> mDrawingPolygonPoints = new ArrayList<>();
    private List<List<LatLng>> mDrawingPolygonHoles = new ArrayList<>();

    private ArrayList<Marker> mAllMarkers = new ArrayList<>();
    private Polygon mMainPolygon, mDrawingPolygon;


    Resources mResources;

    public DrawingPolygonFragment() {
        super();
    }

    public static DrawingPolygonFragment newInstance(
            JsonArray mainPolygon, List<LatLng> drawingPolygon,
            List<List<LatLng>> drawingPolygonHoles, CameraPosition camera) {

        DrawingPolygonFragment fragment = new DrawingPolygonFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARGS_MYLOCATION_ENABLE, true);
        args.putParcelable(CAMERA_POSITION, camera);

        if (mainPolygon != null) {
            fragment.rawMainPolygon = mainPolygon;
            args.putBoolean(DrawingPolygonFragment.ARGS_MOVE_TO_MY_LOCATION, false);
        } else if (drawingPolygon != null) {
            args.putBoolean(DrawingPolygonFragment.ARGS_MOVE_TO_MY_LOCATION, false);
        } else if (camera != null) {
            args.putBoolean(DrawingPolygonFragment.ARGS_MOVE_TO_MY_LOCATION, false);
        } else {
            args.putBoolean(DrawingPolygonFragment.ARGS_MOVE_TO_MY_LOCATION, true);
        }

        fragment.mDrawingPolygonPoints = drawingPolygon;
        fragment.mDrawingPolygonHoles = drawingPolygonHoles;

        fragment.setArguments(args);
        fragment.startEditMode();
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mMap = getMap();
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.setOnMapLongClickListener(mapLongClickListener);
        mMap.setOnMarkerDragListener(markerDragListener);

        mResources = getActivity().getResources();

        mCameraPosition = getArguments().getParcelable(CAMERA_POSITION);

        if (rawMainPolygon != null) {
            mMainPolygon = drawPolygonByGeoJson(rawMainPolygon, 0, Color.parseColor("#FF4081"));
            zoomOnPolygon(mCameraPosition, rawMainPolygon);
        }

        if (mDrawingPolygonPoints == null || mDrawingPolygonPoints.isEmpty()) {
            mDrawingPolygonPoints = new ArrayList<>();

            if (mMainPolygon == null && getCurrentLocation() == null) {
                moveCameraToThailand(getActivity(), mMap);
            }

            if (mCameraPosition != null) {
                Log.d("camera", "camera instance recieved" + mCameraPosition.target);
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
            }
        } else {
            drawDrawingPolygon(mDrawingPolygonPoints, mDrawingPolygonHoles);
        }

    }

    OnMarkerDragListener markerDragListener = new OnMarkerDragListener() {
        @Override
        public void onMarkerDrag(Marker marker) {
            if (editMode) {
                mDrawingPolygonPoints.set(mAllMarkers.indexOf(marker), marker.getPosition());
                mDrawingPolygon = updatePolygon(mDrawingPolygonPoints, mDrawingPolygonHoles);
                mAreaChangedListener.onPolygonChange(
                        mDrawingPolygonPoints, mDrawingPolygonHoles,
                        MapUtils.getCentroid(mDrawingPolygon.getPoints()),
                        SphericalUtil.computeArea(mDrawingPolygon.getPoints()));

            }
        }

        @Override
        public void onMarkerDragEnd(Marker marker) {
            String markerCount = String.valueOf(mAllMarkers.indexOf(marker) + 1);
            if (mMainPolygon != null) {
                marker.setIcon(addMarkerInBoundary(mMainPolygon, marker.getPosition(), markerCount));
            }
        }

        @Override
        public void onMarkerDragStart(Marker marker) {

        }
    };

    OnMapLongClickListener mapLongClickListener = new OnMapLongClickListener() {
        @Override
        public void onMapLongClick(LatLng setMarkerPoint) {
            if (editMode)
                drawEachPointOfPolygon(setMarkerPoint);
        }
    };

    public void drawDrawingPolygon(List<LatLng> polygon, List<List<LatLng>> holes) {
        if (polygon == null)
            return;

        redrawPolygon(polygon, holes);
        if (mMainPolygon == null) {
            zoomOnPolygon(mCameraPosition, polygon);
        }
    }

    // draw editable polygon
    public Polygon updatePolygon(List<LatLng> polygon, List<List<LatLng>> holes) {
        if (mDrawingPolygon != null) {
            mDrawingPolygon.remove();
        }
        setMoveToMyLocation(false);
        return drawPolygonByHand(polygon, holes, 1, Color.parseColor("#FF4081"));
    }

    public void clearDrawingMap() {
        if (mMap != null) {
            mPointListener.onPositionAdded(null);
            if (mDrawingPolygon != null && mMainPolygon != null) {
                mDrawingPolygon.remove();
            } else if (mMainPolygon == null) {
                mMap.clear();
            }
        }
        clearDrawingBoundaryMarkers();
        mDrawingPolygonPoints = new ArrayList<>();

        if (mAreaChangedListener != null) {
            mAreaChangedListener.onPolygonChange(mDrawingPolygonPoints, mDrawingPolygonHoles, null, 0);
        }
    }

    public void startEditMode() {
        editMode = true;
    }

    public void stopEditMode() {
        editMode = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(CAMERA_POSITION, mMap.getCameraPosition());
        outState.putParcelableArrayList(DRAWING_POLYGON, (ArrayList<? extends Parcelable>) mDrawingPolygonPoints);
        outState.putSerializable(DRAWING_POLYGON_HOLES, (Serializable) mDrawingPolygonHoles);
        outState.putBoolean("editMode", editMode);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mCameraPosition = savedInstanceState.getParcelable(CAMERA_POSITION);
            mDrawingPolygonPoints = savedInstanceState.getParcelableArrayList(DRAWING_POLYGON);
            mDrawingPolygonHoles = (List<List<LatLng>>) savedInstanceState.getSerializable(DRAWING_POLYGON_HOLES);
            editMode = savedInstanceState.getBoolean("editMode");

            if (!mDrawingPolygonPoints.isEmpty()) {
                zoomOnPolygon(mCameraPosition, mDrawingPolygonPoints);
                setMoveToMyLocationEnabled(false);
            } else if (mCameraPosition != null) {
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
                setMoveToMyLocationEnabled(false);
            } else {
                setMoveToMyLocationEnabled(true);
            }
            redrawPolygon(mDrawingPolygonPoints, mDrawingPolygonHoles);
        }
    }

    public void drawEachPointOfPolygon(LatLng point) {
        mPointListener.onPositionAdded(point);

        mDrawingPolygonPoints.add(point);
        MarkerOptions mm = getPolygonMarkerOptions(point, mDrawingPolygonPoints.size() - 1);
        mAllMarkers.add(mMap.addMarker(mm));

        mDrawingPolygon = updatePolygon(mDrawingPolygonPoints, mDrawingPolygonHoles);
        mAreaChangedListener.onPolygonChange(mDrawingPolygonPoints, mDrawingPolygonHoles,
                MapUtils.getCentroid(mDrawingPolygonPoints), SphericalUtil.computeArea(mDrawingPolygonPoints));

    }

    public void redrawPolygon(List<LatLng> polygon, List<List<LatLng>> holes) {
        mDrawingPolygonPoints = polygon;

        clearDrawingBoundaryMarkers();
        int currentIndex = 0;
        for (LatLng eachPoint : polygon) {
            if (mMap == null) {
                Log.d("map", "it null");
            } else {
                mAllMarkers.add(mMap.addMarker(getPolygonMarkerOptions(eachPoint, currentIndex)));
                mDrawingPolygon = updatePolygon(polygon, holes);
                if (mAreaChangedListener != null) {
                    mAreaChangedListener.onPolygonChange(polygon, holes,
                            MapUtils.getCentroid(mDrawingPolygonPoints), SphericalUtil.computeArea(mDrawingPolygonPoints));
                }
            }
            currentIndex++;
        }
    }

    private MarkerOptions getPolygonMarkerOptions(LatLng position, int polygon_index) {
        MarkerOptions mm = new MarkerOptions();
        mm.position(position).draggable(true);
        //Create icon
        IconGenerator icon = new IconGenerator(getActivity());
        String markingCount = String.valueOf(polygon_index + 1);

        if (mMainPolygon != null) {
            mm.icon(addMarkerInBoundary(mMainPolygon, position, markingCount));
        } else {
            icon.setStyle(IconGenerator.STYLE_BLUE);
            mm.icon(BitmapDescriptorFactory.fromBitmap(icon.makeIcon(markingCount)));
        }
        return mm;
    }

    public void removeLastPoint(LatLng lastPoint) {
        int pointIndex = mDrawingPolygonPoints.lastIndexOf(lastPoint);
        if (pointIndex != -1) {
            clearDrawingBoundaryMarkers();
            mDrawingPolygonPoints.remove(pointIndex);
            //remove polygon
            mDrawingPolygon.remove();
            redrawPolygon(mDrawingPolygonPoints, mDrawingPolygonHoles);
        }
        mPointListener.onPositionAdded(null);
    }

    public void clearDrawingBoundaryMarkers() {
        if (mAllMarkers.size() < 1)
            return;

        int markerSize = mAllMarkers.size();
        for (int i = 0; i < markerSize; i++) {
            mAllMarkers.get(i).remove();
        }
        mAllMarkers = new ArrayList<>();
    }

    public boolean validateDrawingPolygonInsideStaticPolygon(List<LatLng> drawingPolygon, List<LatLng> backgroundPolygon) {
        for (LatLng eachPoint : drawingPolygon) {
            if (!PolyUtil.containsLocation(eachPoint, backgroundPolygon, false) &&
                    !PolyUtil.isLocationOnEdge(eachPoint, backgroundPolygon, false, 0.5)) {
                return false;
            }
        }
        return true;
    }

    public List<LatLng> getDrawingPolygonPoint() {
        return mDrawingPolygonPoints;
    }

    //callback listener
    public void setOnPolygonChangeListener(OnPolygonChangeListener listener) {
        mAreaChangedListener = listener;
    }

    public void setOnPositionAddedListener(OnPositionAddedListener listener) {
        mPointListener = listener;
    }

    public static interface OnPolygonChangeListener {
        public void onPolygonChange(List<LatLng> polygonArray, List<List<LatLng>> polygonHolesArray, LatLng centroid, double area);
    }

    public static interface OnPositionAddedListener {
        public void onPositionAdded(LatLng lastPoint);
    }
}