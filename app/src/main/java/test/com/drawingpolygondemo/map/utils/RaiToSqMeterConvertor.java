package test.com.drawingpolygondemo.map.utils;


public class RaiToSqMeterConvertor {
    public static double RaiToSqMeter(int rai, int ngan, int tarangwa) {
        double sqMeter = (rai * 1600) + (ngan * 400) + (tarangwa * 4);
        return sqMeter;
    }

    public static int sqMeterToRai(int sqMeter) {
        int rai = sqMeter / 1600;
        return rai;
    }

    public static int sqMeterToNgan(int sqMeter) {
        int ngan = (sqMeter % 1600) / 400;
        return ngan;
    }

    public static int sqMeterToTarangwa(int sqMeter) {
        int tarangwa = (sqMeter % 400) / 4;
        return tarangwa;
    }

    @Deprecated
    public static String sortArea(int rai, int ngan, int tarangwa) {
        String area = "";
        if (rai > 0) {
            area += rai + " ไร่ ";
        }
        if (ngan > 0) {
            area += ngan + " งาน ";
        }
        if (tarangwa > 0) {
            area += tarangwa + " ตารางวา";
        }
        return area;
    }

    public static String parseArea(int rai, int ngan, int tarangwa) {
        String area = "";
        if (rai > 0) {
            area += rai + " ไร่ ";
        }
        if (ngan > 0) {
            area += ngan + " งาน ";
        }
        if (tarangwa > 0) {
            area += tarangwa + " ตารางวา";
        }
        return area;
    }

    public static String parseArea(int areaSqM) {
        int rai = RaiToSqMeterConvertor.sqMeterToRai(areaSqM);
        int ngan = RaiToSqMeterConvertor.sqMeterToNgan(areaSqM);
        int tarangwa = RaiToSqMeterConvertor.sqMeterToTarangwa(areaSqM);

        return RaiToSqMeterConvertor.parseArea(rai, ngan, tarangwa);
    }
}
