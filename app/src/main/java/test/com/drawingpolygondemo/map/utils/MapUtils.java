/*  TAMIS Android Project 
 *
 *  ***********************************************************************
 *                                                                 _ _ _
 *                                                               ( _ _  |
 *                                                           _ _ _ _  | |
 *                                                          (_ _ _  | |_|
 *  _     _   _ _ _ _     _ _ _   _ _ _ _ _   _ _ _ _     _ _ _   | | 
 * |  \  | | |  _ _ _|   /  _ _| |_ _   _ _| |  _ _ _|   /  _ _|  | |
 * | | \ | | | |_ _ _   /  /         | |     | |_ _ _   /  /      |_|
 * | |\ \| | |  _ _ _| (  (          | |     |  _ _ _| (  (    
 * | | \ | | | |_ _ _   \  \_ _      | |     | |_ _ _   \  \_ _ 
 * |_|  \__| |_ _ _ _|   \_ _ _|     |_|     |_ _ _ _|   \_ _ _| 
 *  a member of NSTDA, @Thailand
 *  
 * ***********************************************************************
 *
 * Copyright (C) 2557-2021 National Electronics and Computer Technology Center
 * All Rights Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * 
 */

package test.com.drawingpolygondemo.map.utils;

import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

/**
 * add description here!
 *
 * @author Choatravee
 * @version 1.0
 */
public class MapUtils {

    public static LatLng getCentroid(List<LatLng> points) {
        double[] tmpCentroid = {0.0, 0.0};

        for (int i = 0; i < points.size(); i++) {
            tmpCentroid[0] += points.get(i).latitude;
            tmpCentroid[1] += points.get(i).longitude;
        }

        int totalPoints = points.size();
        LatLng centroid = new LatLng(tmpCentroid[0] / totalPoints, tmpCentroid[1] / totalPoints);
        return centroid;
    }

    //convert boundary arraylist to short term of json string
    public static String convertBoundaryListToJsonString(ArrayList<LatLng> boundary) {
        Gson gson = new Gson();
        JsonArray boundaryArray = new JsonArray();
        for (int i = 0; i < boundary.size(); i++) {
            JsonObject boundaryElement = new JsonObject();
            boundaryElement.add("lat", gson.toJsonTree(boundary.get(i).latitude));
            boundaryElement.add("lng", gson.toJsonTree(boundary.get(i).longitude));
            boundaryElement.add("no", gson.toJsonTree(i));
            boundaryArray.add(boundaryElement);
        }
        return !boundaryArray.isJsonNull() ? boundaryArray.toString() : null;
    }

    //convert from json string to boundary arraylist
    public static ArrayList<LatLng> convertJsonStringToBoundaryList(String rawBoundary) {
        ArrayList<LatLng> polygonBoundary = new ArrayList<LatLng>();
        if (!TextUtils.isEmpty(rawBoundary)) {
            JsonArray queryBoundary = (JsonArray) new JsonParser().parse(rawBoundary);

            for (int i = 0; i < queryBoundary.size(); i++) {
                JsonObject eachPoint = (JsonObject) new JsonParser().parse(queryBoundary.get(i).toString());
                double pointLatitude = eachPoint.get("lat").getAsDouble();
                double pointLongitude = eachPoint.get("lng").getAsDouble();
                LatLng bound = new LatLng(pointLatitude, pointLongitude);

                polygonBoundary.add(bound);
            }
        }
        return polygonBoundary;
    }

    //convert boundary arraylist to GeoJSON
    public static String convertBoundaryListToGeoJSON(List<LatLng>... boundaries) {
        Gson gson = new Gson();
        JsonArray polygonArray = new JsonArray();

        for (List<LatLng> boundary : boundaries) {
            JsonArray eachPolygon = new JsonArray();
            for (int i = 0; i < boundary.size(); i++) {
                JsonArray boundaryElement = new JsonArray();
                boundaryElement.add(gson.toJsonTree(boundary.get(i).longitude));
                boundaryElement.add(gson.toJsonTree(boundary.get(i).latitude));
                eachPolygon.add(boundaryElement);
            }
            polygonArray.add(eachPolygon);
        }
        return !polygonArray.isJsonNull() ? polygonArray.toString() : null;
    }

    //convert from GeoJSON to boundary arraylist
    public static List<List<LatLng>> convertGeoJSONToBoundaryList(String rawBoundary) {

        List<List<LatLng>> polygonArray = new ArrayList<>();

        if (!TextUtils.isEmpty(rawBoundary)) {
            JsonArray queryBoundaries = (JsonArray) new JsonParser().parse(rawBoundary);
            ArrayList<LatLng> polygonBoundary = new ArrayList<LatLng>();
            int polygonCount = queryBoundaries.size();
            if (!queryBoundaries.isJsonNull()) {
                for (int i = 0; i < polygonCount; i++) {
                    JsonArray queryBoundary = (JsonArray) queryBoundaries.get(0);
                    int pointCount = queryBoundary.size();
                    for (int j = 0; j < pointCount; j++) {
                        JsonArray eachPoint = (JsonArray) queryBoundary.get(j);
                        double pointLongitude = eachPoint.get(0).getAsDouble();
                        double pointLatitude = eachPoint.get(1).getAsDouble();
                        LatLng point = new LatLng(pointLatitude, pointLongitude);
                        polygonBoundary.add(point);
                    }
                }
            }
            polygonArray.add(polygonBoundary);
        }

        return polygonArray;
    }

    public static List<LatLng> getMainPolygon(JsonArray geoJson){
        return extractPolygon(geoJson, 0);
    }

    public static List<List<LatLng>> getPolygonHoles(JsonArray geoJson){
        if(geoJson==null)
            return null;

        List<List<LatLng>> holesPolygon = convertGeoJSONToBoundaryList(geoJson.toString());
        holesPolygon.remove(0);
        return holesPolygon;
    }

    public static List<List<LatLng>> getPolygonHoles(List<List<LatLng>> fullPolygon){
        fullPolygon.remove(0);
        return fullPolygon;
    }

    public static ArrayList<LatLng> extractPolygon(JsonArray geoJson, int position) {
        if(geoJson==null)
            return null;

        ArrayList<LatLng> polygonBoundary = new ArrayList<>();
        JsonArray queryBoundary = (JsonArray) geoJson.get(position);
        int pointCount = queryBoundary.size();
        for (int j = 0; j < pointCount; j++) {
            JsonArray eachPoint = (JsonArray) queryBoundary.get(j);
            double pointLongitude = eachPoint.get(0).getAsDouble();
            double pointLatitude = eachPoint.get(1).getAsDouble();
            LatLng point = new LatLng(pointLatitude, pointLongitude);
            polygonBoundary.add(point);
        }
        return polygonBoundary;
    }

    //convert boundary arraylist to GeoJSON
    public static JsonArray buildGeoJSON(List<LatLng> polygon, List<List<LatLng>> holes) {

        JsonArray geoJson = new JsonArray();

        if(polygon==null && polygon.isEmpty())
            return null;
        
        geoJson.add(buildGeoJsonElements(polygon));

        if(holes!=null){
            for (List<LatLng> eachHole : holes) {
                JsonArray eachGeoJsonHole = buildGeoJsonElements(eachHole);
                geoJson.add(eachGeoJsonHole);
            }
        }
        return geoJson;
    }

    public static JsonArray buildGeoJsonElements(List<LatLng> boundary) {
        JsonArray eachPolygon = new JsonArray();
        for (int i = 0; i < boundary.size(); i++) {
            Gson gson = new Gson();
            JsonArray boundaryElement = new JsonArray();
            boundaryElement.add(gson.toJsonTree(boundary.get(i).longitude));
            boundaryElement.add(gson.toJsonTree(boundary.get(i).latitude));
            eachPolygon.add(boundaryElement);
        }
        return eachPolygon;
    }

    //convert from GeoJSON to boundary arraylist
    public static JsonArray parseGeoJSONString(String rawBoundary) {

        return (!TextUtils.isEmpty(rawBoundary))
                ? (JsonArray) new JsonParser().parse(rawBoundary)
                : null;
    }
}
