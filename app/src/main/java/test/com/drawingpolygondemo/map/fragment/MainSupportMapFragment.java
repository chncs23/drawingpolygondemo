package test.com.drawingpolygondemo.map.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.JsonArray;
import com.google.maps.android.MarkerManager;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.List;

import test.com.drawingpolygondemo.R;
import test.com.drawingpolygondemo.map.utils.MapUtils;


public class MainSupportMapFragment extends SupportMapFragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnCameraChangeListener {

    private GoogleMap mMap;

    public MainSupportMapFragment() {
        super();
    }

    public static final String ARGS_LOCKED = "locked";
    public static final String ARGS_ZOOMABLE = "zoomable";
    public static final String ARGS_MYLOCATION_ENABLE = "myLocationEnabled";
    public static final String ARGS_MOVE_TO_MY_LOCATION = "moveToMyLocation";

    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 1;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL = 500;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL = 100;

    private Boolean isMoveToMyLocation = false;
    private Boolean isLocked = false;
    private Boolean isZoomable = false;
    private Boolean isGPSDialogShowed = false;
    private Boolean isLocationEnabled = true;

    private Location mCurrentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    OnPositionChangeListener mLocationListener;
    OnCurrentCameraChangeListener mCameraChangeListener;

    MarkerManager markerManager;
    MarkerManager.Collection mMarkerCollection;

    private ArrayList<Polygon> mPolygonCollection;

    private int mPolygonZIndex = 0;

    public static LatLngBounds THAILAND = new LatLngBounds(new LatLng(5, 97),
            new LatLng(21, 105));


    /**
     * New instance of tamis map fragment with these parameter
     * Map Locked is true
     * LocationEnabled is true
     * MoveToMyLocation is true
     *
     * @return instance of TAMISMapFragment.
     */
    public static MainSupportMapFragment newInstance() {
        return newInstance(false, true, true, false);
    }

    /**
     * Initiate map fragment.
     *
     * @param isLocked           set map to lock
     * @param isLocationEnabled  enabled location in map
     * @param isMovetoMyLocation move position automatically when position updated
     * @return
     */
    public static MainSupportMapFragment newInstance(boolean isLocked, boolean isZoomable, boolean isLocationEnabled, boolean isMovetoMyLocation) {
        MainSupportMapFragment mapFrag = new MainSupportMapFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARGS_LOCKED, isLocked);
        args.putBoolean(ARGS_MYLOCATION_ENABLE, isLocationEnabled);
        args.putBoolean(ARGS_MOVE_TO_MY_LOCATION, isMovetoMyLocation);
        args.putBoolean(ARGS_ZOOMABLE, isZoomable);

        mapFrag.setArguments(args);
        return mapFrag;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            isLocked = args.getBoolean(ARGS_LOCKED, false);
            isZoomable = args.getBoolean(ARGS_ZOOMABLE, true);
            isLocationEnabled = args.getBoolean(ARGS_MYLOCATION_ENABLE, true);
            isMoveToMyLocation = args.getBoolean(ARGS_MOVE_TO_MY_LOCATION, isLocationEnabled);
        }

        mMap = getMap();
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.setOnCameraChangeListener(this);
        markerManager = new MarkerManager(mMap);
        mMarkerCollection = markerManager.newCollection();
        mPolygonCollection = new ArrayList<Polygon>();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        setLockedMap(isLocked);
        mMap.getUiSettings().setZoomControlsEnabled(isZoomable);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map_normal:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.map_satellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.map_terrain:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setMoveToMyLocation(boolean isMoveToMyLocation){
        this.isMoveToMyLocation = isMoveToMyLocation;
    }

    public class MyItem implements ClusterItem {
        private final LatLng mPosition;

        public MyItem(double lat, double lng) {
            mPosition = new LatLng(lat, lng);
        }

        @Override
        public LatLng getPosition() {
            return mPosition;
        }
    }

    public void setPolygonZIndex(int zIndex) {
        mPolygonZIndex = zIndex;
    }

    protected BitmapDescriptor addMarkerInBoundary(Polygon polygon, LatLng position, String text) {
        IconGenerator icon = new IconGenerator(getActivity());
        if (!PolyUtil.containsLocation(position, polygon.getPoints(), false) &&
                !PolyUtil.isLocationOnEdge(position, polygon.getPoints(), false, 0.5)) {
            icon.setStyle(IconGenerator.STYLE_RED);
            return BitmapDescriptorFactory.fromBitmap(icon.makeIcon(text));
        } else {
            icon.setStyle(IconGenerator.STYLE_BLUE);
            return BitmapDescriptorFactory.fromBitmap(icon.makeIcon(text));
        }
    }

    public void drawPolygon(ArrayList<LatLng> boundary) {
        if (boundary.size() > 2) {
            clearPolygon();
            PolygonOptions polygon = new PolygonOptions();
            polygon.addAll(boundary);
            onCreatePolygon(polygon, mPolygonZIndex);

            mPolygonCollection.add(mMap.addPolygon(polygon));
            zoomOnPolygon(null, boundary);
        } else {
            Toast.makeText(getActivity(),"ไม่สามารถวาดพื้นที่แปลงได้เนื่องจากกำหนดจุดวาดในพื้นที่น้อยกว่า 3 จุด", Toast.LENGTH_LONG).show();
        }
    }

    public void drawPolygon(String jsonBoundary) {
        if (TextUtils.isEmpty(jsonBoundary)) {
            Log.d("map frag", "json boundaray is empty");
            return;
        }
        ArrayList<LatLng> mPolygonBoundary = MapUtils.convertJsonStringToBoundaryList(jsonBoundary);
        drawPolygon(mPolygonBoundary);
    }

    public Polygon drawPolygon(List<LatLng> boundary, int zIndex, int polygonColor) {
        if (boundary.size() > 2) {
            return drawPolygon(boundary, null, zIndex, polygonColor);
        } else {
            Toast.makeText(getActivity(), "ไม่สามารถวาดพื้นที่แปลงได้เนื่องจากกำหนดจุดวาดในพื้นที่น้อยกว่า 3 จุด", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public Polygon drawPolygonByHand(List<LatLng> boundary, List<List<LatLng>> holes, int zIndex, int polygonColor) {
        return drawPolygon(boundary, holes, zIndex, polygonColor);
    }

    public Polygon drawPolygonWithHoles(List<LatLng> boundary, List<List<LatLng>> holes, int zIndex, int polygonColor) {
        if (boundary.size() > 2) {
            return drawPolygon(boundary, holes, zIndex, polygonColor);
        } else {
            Toast.makeText(getActivity(), "ไม่สามารถวาดพื้นที่แปลงได้เนื่องจากกำหนดจุดวาดในพื้นที่น้อยกว่า 3 จุด", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public Polygon drawPolygonByGeoJson(JsonArray geoJson, int zIndex, int polygonColor) {
        if (geoJson == null)
            return null;

        ArrayList<LatLng> boundary = new ArrayList<>();
        List<List<LatLng>> holes = new ArrayList<>();
        int polygonCount = geoJson.size();
        if (!geoJson.isJsonNull()) {
            for (int i = 0; i < polygonCount; i++) {
                ArrayList<LatLng> polygonBoundary = MapUtils.extractPolygon(geoJson, i);

                if (i == 0) {
                    boundary = polygonBoundary;
                } else {
                    holes.add(polygonBoundary);
                }
            }
        }

        return drawPolygon(boundary, holes, zIndex, polygonColor);
    }

    public Polygon drawPolygon(List<LatLng> boundary, List<List<LatLng>> holes, int zIndex, int polygonColor) {
        PolygonOptions polygonOptions = new PolygonOptions();
        polygonOptions.addAll(boundary);
        onCreatePolygon(polygonOptions, zIndex, polygonColor);

        Polygon polygon = mMap.addPolygon(polygonOptions);
        if (holes != null && !holes.isEmpty())
            polygon.setHoles(holes);
        mPolygonCollection.add(polygon);

        return mPolygonCollection.get(mPolygonCollection.size() - 1);
    }

    protected void onCreatePolygon(PolygonOptions polygonOption, int index) {
        polygonOption.fillColor(Color.parseColor("#330099FF"));
        polygonOption.strokeColor(Color.parseColor("#0099FF"));
        polygonOption.strokeWidth(4);
        polygonOption.zIndex(index);
    }

    protected void onCreatePolygon(PolygonOptions polygonOption, int index, int polygonColor) {
        polygonOption.fillColor((160 << 24) | (polygonColor & 0x00ffffff)); //make it transparent
        polygonOption.strokeColor(polygonColor);
        polygonOption.strokeWidth(4);
        polygonOption.zIndex(index);
    }

    // Draw multiple polygon
    public void drawPolygons(ArrayList<LatLng>... boundary) {
        clearPolygon();
        for (int i = 0; i < boundary.length; i++) {
            // first polygon is site boundary

            if (boundary[i] == null)
                return;

            if (boundary[i].size() > 2) {
                PolygonOptions polygon = new PolygonOptions();
                polygon.addAll(boundary[i]);
                onCreatePolygon(polygon, i);

                mPolygonCollection.add(mMap.addPolygon(polygon));

                if (i == 0) {
                    zoomOnPolygon(null, boundary[0]);
                }
            } else if (!boundary[i].isEmpty()) {
                Toast.makeText(getActivity(), "ไม่สามารถวาดพื้นที่แปลงได้เนื่องจากกำหนดจุดวาดในพื้นที่น้อยกว่า 3 จุด", Toast.LENGTH_LONG).show();
            }
        }
    }

    //add marker
    public void addMarker(LatLng position) {
        addMarker(position, Color.parseColor("#039BE5"));
    }

    public void addMarker(LatLng position, int color) {
        float hsv[] = new float[3];
        Color.colorToHSV(color, hsv);

        MarkerOptions marker = new MarkerOptions();
        marker.icon(BitmapDescriptorFactory.defaultMarker(hsv[0]));
        marker.position(position);

        mMarkerCollection.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 18));
    }

    public Marker getMarker(int position) {
        return new ArrayList<>(mMarkerCollection.getMarkers()).get(position);
    }

    public void clearMarkers() {
        mMarkerCollection.clear();
    }

    public void clearMap() {
        mMap.clear();
    }

    public void clearPolygon() {
        for (int i = 0; i < mPolygonCollection.size(); i++) {
            mPolygonCollection.get(i).remove();
        }
        mPolygonCollection = new ArrayList<>();
    }

    public ArrayList<Polygon> getPolygonCollection() {
        return mPolygonCollection;
    }

    public void zoomOnPolygon(CameraPosition cameraPosition, JsonArray geoJson) {
        zoomOnPolygon(cameraPosition, MapUtils.getMainPolygon(geoJson));
    }

    public void zoomOnPolygon(CameraPosition cameraPosition, List<LatLng> boundary) {
        CameraUpdate cu;
        if (cameraPosition != null) {
            cu = CameraUpdateFactory.newCameraPosition(cameraPosition);
        } else {
            LatLngBounds.Builder zoomArea = new LatLngBounds.Builder();
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int height = metrics.heightPixels;
            int width = metrics.widthPixels;

            int padding = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 64, getResources()
                            .getDisplayMetrics());
            for (LatLng eachCorner : boundary) {
                zoomArea.include(eachCorner);
            }
            LatLngBounds bounds = zoomArea.build();
            cu = CameraUpdateFactory.newLatLngBounds(bounds, width,
                    height, padding);
        }
        mMap.moveCamera(cu);
    }

    public void setLockedMap(boolean isLocked) {
        if (isLocked) {
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        } else {
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
    }

    public void setLocationEnabled(boolean isLocationEnabled) {
        mMap.setMyLocationEnabled(isLocationEnabled);

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * @return current location from GooglePlay Service Location Client, May
     * null if Can't connect the service
     * @since 1.0
     */
    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    public void setMoveToMyLocationEnabled(boolean enabled) {
        isMoveToMyLocation = enabled;
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        Toast.makeText(getActivity(), "ไม่สามารถเชื่อมต่อ GooglePlayServices ได้", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        setLocationEnabled(isLocationEnabled);

        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mCurrentLocation != null && isMoveToMyLocation) {
            Log.d("FOUND", "Current Location IS FOUND");
            onMoveToLocation(mCurrentLocation);
        } else {
            if (isMoveToMyLocation) {
                Log.d("NULL", "Current Location is NULL");

                if (!isGPSDialogShowed) {
                    showGpsSettingsDialog();
                    isGPSDialogShowed = true;
                }

                moveCameraToThailand(getActivity(), mMap);
            } else {
                Log.d("DISABLED", "Current Location is disabled");
            }
        }
    }

    /**
     * Show gps setting dialog when gps disable
     */
    public void showGpsSettingsDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("GPS ยังไม่ได้เปิดการใช้งาน");

        // Setting Dialog Message
        alertDialog
                .setMessage("คุณต้องการเปิดการตั้งค่าการใช้งาน GPS หรือไม่");

        // On pressing Settings button
        alertDialog.setPositiveButton("เปิดการตั้งค่า GPS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });

        // on pressing cancel button
        alertDialog.setNegativeButton("ยกเลิก",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // Showing Alert Message
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
        if (mLocationListener != null) {
            mLocationListener.onPositionChange(position);
        }
    }

    @Override
    public void onCameraChange(CameraPosition position) {
        if (mCameraChangeListener != null) {
            mCameraChangeListener.setOnCameraChangeListener(position);
        }
    }

    public static void moveCameraToThailand(Activity activity, GoogleMap map) {
        if (map == null)
            return;

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay()
                .getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(THAILAND, width,
                height, 32));
    }


    public void onMoveToLocation(Location location) {
        LatLng cur = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cur, 15));
    }

    public void moveToLocation(LatLng position) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
    }

    //callback listener
    public void setOnPositionChangeListener(OnPositionChangeListener listener) {
        mLocationListener = listener;
    }

    public void setOnCameraChangeListener(OnCurrentCameraChangeListener listener) {
        mCameraChangeListener = listener;
    }


    public static interface OnPositionChangeListener {
        public void onPositionChange(LatLng position);
    }

    public static interface OnCurrentCameraChangeListener {
        public void setOnCameraChangeListener(CameraPosition position);

    }
}
